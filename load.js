require("@babel/register");
require("@babel/polyfill");

// wait for 100 seconds to make kafka connection
setTimeout(() => {
	require('./load_data.js');
}, 1000);